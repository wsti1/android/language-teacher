package com.example.language_teacher;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.language_teacher.word.Word;
import com.example.language_teacher.word.WordRepository;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class ContentActivity extends AppCompatActivity implements View.OnClickListener {

    private String selectedLanguage;
    private String selectedMode;

    private TextView indexText;

    private EditText wordText;
    private EditText guessText;

    private List<Word> words;
    private ListIterator<Word> iterator;
    private int correctAnswersCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        Bundle bundle = Objects.requireNonNull(getIntent().getExtras());
        selectedLanguage = bundle.getString("language");
        selectedMode = bundle.getString("mode");

        indexText = findViewById(R.id.index_word);
        wordText = findViewById(R.id.word_text);
        guessText = findViewById(R.id.guess_text);
        if ("nauka".equals(selectedMode)) {
            guessText.setFocusable(false);
        }
        findViewById(R.id.exit_program).setOnClickListener(this);
        findViewById(R.id.next_word).setOnClickListener(this);

        words = WordRepository.getShuffledWords();
        iterator = words.listIterator();
        correctAnswersCount = 0;
        updateContent(iterator.next());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exit_program:
                showExitDialog();
                break;
            case R.id.next_word:
                if ("test".equals(selectedMode)) {
                    updateCorrectAnswersCount();
                }
                Word word = iterator.next();
                if (!iterator.hasNext()) {
                    if ("nauka".equals(selectedMode)) {
                        iterator = words.listIterator();
                    } else {
                        finish();
                        startActivity(createIntentForResultActivity());
                    }
                }
                updateContent(word);
                break;
        }
    }

    private void showExitDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Wyjście")
                .setMessage("Czy na pewno chcesz wyjść?")
                .setPositiveButton("Tak", (dialog, which) -> {
                    finish();
                    dialog.dismiss();
                })
                .setNegativeButton("Nie", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void updateCorrectAnswersCount() {
        Word currentWord = words.get(iterator.nextIndex() - 1);
        if (currentWord.getTranslation(selectedLanguage).contentEquals(guessText.getText())) {
            correctAnswersCount++;
        }
    }

    private Intent createIntentForResultActivity() {
        Intent intent = new Intent(this , ResultActivity.class);
        intent.putExtra("result", correctAnswersCount + "/" + words.size());
        return intent;
    }

    private void updateContent(Word word) {
        String wordNumbering = (words.indexOf(word) + 1) + "/" + words.size();
        indexText.setText(wordNumbering);

        switch (selectedLanguage) {
            case "polski":
                updateWord(word.getEnglish(), word.getPolish());
                break;
            case "angielski":
                updateWord(word.getPolish(), word.getEnglish());
                break;
        }
    }

    private void updateWord(String word, String guess) {
        wordText.setText(word);
        if ("nauka".equals(selectedMode)) {
            guessText.setText(guess);
        } else {
            guessText.setText("");
        }
    }
}
