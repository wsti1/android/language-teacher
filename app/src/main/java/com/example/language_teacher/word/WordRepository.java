package com.example.language_teacher.word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WordRepository {

    private static List<Word> words;

    static {
        words = new ArrayList<>();
        words.add(new Word("apple","jabłko"));
        words.add(new Word("apricot","morela"));
        words.add(new Word("aubergine","bakłażan"));
        words.add(new Word("bacon","bekon"));
        words.add(new Word("baguette","bagietka"));
        words.add(new Word("banana","banan"));
        words.add(new Word("bean","fasola"));
        words.add(new Word("beef","wołowina"));
        words.add(new Word("blueberry","jagoda"));
        words.add(new Word("biscuit","herbatnik"));
        words.add(new Word("blackberry","jeżyna"));
        words.add(new Word("bread","chleb"));
        words.add(new Word("butter","masło"));
    }

    public static List<Word> getShuffledWords() {
        List<Word> copiedWords = getWords();
        Collections.shuffle(copiedWords);
        return copiedWords;
    }

    public static List<Word> getWords() {
        return new ArrayList<>(words);
    }
}
