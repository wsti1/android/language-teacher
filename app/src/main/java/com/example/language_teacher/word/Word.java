package com.example.language_teacher.word;

public class Word {

    private String english;
    private String polish;

    Word(String english, String polish) {
        this.english = english;
        this.polish = polish;
    }

    public String getEnglish() {
        return english;
    }

    public String getPolish() {
        return polish;
    }

    public String getTranslation(String language) {
        switch (language) {
            case "angielski":
                return english;
            case "polski":
                return polish;
        }
        throw new IllegalStateException("Unsupported language: " + language);
    }
}
