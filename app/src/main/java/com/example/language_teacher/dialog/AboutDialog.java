package com.example.language_teacher.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class AboutDialog extends AppCompatDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        builder.setTitle("O programie")
                .setMessage(new StringBuilder()
                        .append("Program: Language Teacher\n")
                        .append("Wersja: v1.0.0\n")
                        .append("Autor: Damian Kostrzewski"))
                .setPositiveButton("ok", (dialog, which) -> {});
        return builder.create();
    }
}