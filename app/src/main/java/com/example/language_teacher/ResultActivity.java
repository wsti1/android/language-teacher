package com.example.language_teacher;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.MessageFormat;
import java.util.Objects;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle bundle = Objects.requireNonNull(getIntent().getExtras());
        String testResult = bundle.getString("result");

        ((TextView) findViewById(R.id.result_text))
                .setText(MessageFormat.format("Twój wynik to {0}", testResult));

        findViewById(R.id.exit_program).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (R.id.exit_program == v.getId()) {
            finish();
        }
    }
}
