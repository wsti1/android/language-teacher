package com.example.language_teacher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.language_teacher.dialog.AboutDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.start_program).setOnClickListener(this);
        findViewById(R.id.about_program).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_program:
                startActivity(createIntentForContentActivity());
                break;
            case R.id.about_program:
                showAboutDialog();
                break;
        }
    }

    private Intent createIntentForContentActivity() {
        Intent intent = new Intent(this , ContentActivity.class);
        intent.putExtra("language", getSelectedRadioButton(R.id.language_group).getText());
        intent.putExtra("mode", getSelectedRadioButton(R.id.mode_group).getText());
        return intent;
    }

    private RadioButton getSelectedRadioButton(int id) {
        return findViewById(((RadioGroup) findViewById(id)).getCheckedRadioButtonId());
    }

    public void showAboutDialog(){
        AboutDialog dialog = new AboutDialog();
        dialog.show(getSupportFragmentManager(),"AboutDialog");
    }
}
